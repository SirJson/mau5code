#include "Mau5Code.h"

#include <QtGui/QLabel>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QAction>
#include <QtGui/QFileDialog>

#include "CppManager.h"
#include "Utility.h"
#include "CodeDocument.h"

Mau5Code::Mau5Code()
{
	clangMgr = new CppManager();
}

Mau5Code::~Mau5Code()
{
	for (CodeDocument* document : documents) {
		delete document;
	}
	delete clangMgr;
}

void Mau5Code::Setup(Ui::MainWindow* window)
{
	userInterface = window;
	//this->documents.push_back(new CodeDocument(window,clangMgr,"main.cpp"));
	//currentDocument = this->documents[this->documents.size() - 1];
	
	connect(window->editorTabs, SIGNAL(currentChanged(int)),this, SLOT(tabChanged(int)));
	connect(window->actionSave, SIGNAL(triggered()),this, SLOT(saveTriggered()));
	connect(window->actionOpen, SIGNAL(triggered()),this, SLOT(openTriggered()));
	/*
	clangMgr->AddFile("main.cpp");
	clangMgr->Parse();

	
	
	std::vector<Diagnostic> list = clangMgr->GetDiagnostics();
	for (Diagnostic diagnostic : list) {
		window->errorList->addItem(diagnostic.Text.c_str());
		window->textEdit->fillIndicatorRange(diagnostic.Line - 1,0,diagnostic.Line - 1,window->textEdit->lineLength(diagnostic.Line - 1),1);
		window->textEdit->annotate(diagnostic.Line - 1,diagnostic.Spelling.c_str(),1);
	}*/
}

void Mau5Code::tabChanged(int index)
{
	if(!openState)
		currentDocument = this->documents[index];
}

void Mau5Code::saveTriggered()
{
	currentDocument->Save();
}

void Mau5Code::openTriggered()
{
	QString fileName = QFileDialog::getOpenFileName(this,tr("Open Code File"),tr("."),tr("Code Files (*.cpp *.h *.php)"));
	if (!fileName.isEmpty())
	{
		openState = true;
		this->documents.push_back(new CodeDocument(this,clangMgr,fileName.toStdString()));
		currentDocument = this->documents[this->documents.size() - 1];
		openState = false;
	}
}

#include "Mau5Code.moc"
