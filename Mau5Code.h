#ifndef Mau5Code_H
#define Mau5Code_H

#include <QtGui/QMainWindow>
#include "UI/mainUI.h"

class LanguageManager;
class CodeDocument;

class Mau5Code : public QMainWindow
{
Q_OBJECT
public:
	Mau5Code();
	void Setup(Ui::MainWindow* window);
	virtual ~Mau5Code();
public:
	Ui::MainWindow* GetUserInterface() const { return userInterface; }
private slots:
	void tabChanged(int index);
	void saveTriggered();
	void openTriggered();
private:
	Ui::MainWindow* userInterface;
	LanguageManager* clangMgr;
	std::vector<CodeDocument*> documents;
	CodeDocument* currentDocument;
	bool openState;
};

#endif // Mau5Code_H
