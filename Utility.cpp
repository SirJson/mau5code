#include "Utility.h"
#include "Exception.h"
#include <sstream>
#include <fstream>

std::string ReadFileToString(std::string file)
{
	std::string output = "";
    std::ifstream stream(file.c_str(), std::ios::in);
    if(stream.is_open()) {
        std::string Line = "";
		if(std::getline(stream, Line)) {
			output += Line;
			while(std::getline(stream, Line))
				output += "\n" + Line;
			stream.close();
		}
    }
	else {
		throw FileNotFoundException(file);
	}
	return output;
}

std::string WriteFileFromString(std::string data, std::string file)
{
	std::ofstream stream;
	stream.open(file);
	stream << data;
	stream.close();
}

std::string DiagnosticTypeToString(DiagnosticType type)
{
	if(type == DiagnosticType::Error)
		return "Error: ";
	else if(type == DiagnosticType::Fatal)
		return "Fatal: ";
	else if(type == DiagnosticType::Ignored)
		return "Ignored: ";
	else if(type == DiagnosticType::Note)
		return "Note: ";
	else if(type == DiagnosticType::Warning)
		return "Warning: ";
	else
		return "Unknown: ";
}