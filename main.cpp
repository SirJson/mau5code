#include <QtGui/QApplication>
#include "Mau5Code.h"
#include "UI/mainUI.h"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	Mau5Code* appWindow = new Mau5Code();
	Ui::MainWindow* mainWindow = new Ui::MainWindow();
	mainWindow->setupUi(appWindow);
	appWindow->Setup(mainWindow);
	appWindow->show();
	int returnCode = app.exec();
	delete mainWindow;
	delete appWindow;
	return returnCode;
}
