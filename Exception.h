#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>

struct FileNotFoundException : std::exception
{
	FileNotFoundException(std::string file) : file(file) {}
	~FileNotFoundException() throw() {}
	virtual const char* what() const throw() { return ("File not found: " + file).c_str(); }
	std::string file;
};

#endif