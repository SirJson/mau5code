/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Raffael Zica <raffael@trollgames.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CODEDOCUMENT_H
#define CODEDOCUMENT_H

#include "UI/mainUI.h"
#include <qsciscintilla.h>
#include <thread>
#include <map>
#include "LanguageManager.h"

struct Annotation
{
	std::string Text;
	int StartLine, StartColumn;
	int EndLine, EndColumn;
};

class Mau5Code;

class CodeDocument : public QObject
{
	Q_OBJECT
public:
	CodeDocument(Mau5Code* mainApp, LanguageManager* langMgr);
	CodeDocument(Mau5Code* mainApp, LanguageManager* langMgr, std::string file);
	virtual ~CodeDocument();
	bool Save();
	bool MaybeSave();
private:
	bool load();
	void init();
	void parseDocument();
	void buildDiagnostics();
private slots:
	void documentWasModified();
	void errorIndicatorClicked(int line, int index, Qt::KeyboardModifiers state);
	void cursorPositionChanged(int line, int index);
private:
	QWidget* page;
	QsciScintilla* textEdit;
	QHBoxLayout* layout;
	Ui::MainWindow* parentWindow;
	Mau5Code* mainApp;
	std::string filename;
	LanguageManager* langMgr;
	std::thread* parserThread;
	std::mutex langMgrMutex;
	std::vector<Annotation> annotations;
	std::vector<Diagnostic> diagnostics;
};

#endif // CODEDOCUMENT_H
