/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Raffael Zica <raffael@trollgames.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CppManager.h"

CppManager::CppManager()
{
	mainIndex = clang_createIndex(0,0);
}

CppManager::~CppManager()
{
	for(auto translationUnitPair : translationUnits)
		clang_disposeTranslationUnit(translationUnitPair.second);
	clang_disposeIndex(mainIndex);
}

void CppManager::Parse(std::string file)
{
	if(translationUnits.count(file) <= 0)
	{
		std::vector<const char*> commandLine;
		commandLine.push_back(file.c_str());
		translationUnits[file] = clang_parseTranslationUnit(mainIndex,0,&commandLine[0],1,0,0,CXTranslationUnit_None);
	}
	else
		clang_reparseTranslationUnit(translationUnits[file],0,0,CXTranslationUnit_None);
	
	diagnostics[file].clear();
	
	std::vector<Diagnostic> tuDiagnostics = std::vector<Diagnostic>();
		
	for (unsigned int i = 0, n = clang_getNumDiagnostics(translationUnits[file]); i != n; ++i) {
		Diagnostic diagnostic;
		CXDiagnostic clangDiag = clang_getDiagnostic(translationUnits[file], i);
		CXString string = clang_formatDiagnostic(clangDiag, clang_defaultDiagnosticDisplayOptions());
		CXSourceLocation sourceLocation = clang_getDiagnosticLocation(clangDiag);
		CXString spelling = clang_getDiagnosticSpelling(clangDiag);
		
		clang_getInstantiationLocation(sourceLocation,0,&diagnostic.Line,&diagnostic.Column,0);
		diagnostic.Text = clang_getCString(string);
		diagnostic.Type = (DiagnosticType)clang_getDiagnosticSeverity(clangDiag);
		diagnostic.Spelling = clang_getCString(spelling);
		
		for(unsigned int rangeIndex = 0, rangeNum = clang_getDiagnosticNumRanges(clangDiag); rangeIndex != rangeNum; ++rangeIndex)
		{
			Range range;
			CXSourceRange diagRange = clang_getDiagnosticRange(clangDiag,rangeIndex);
			CXSourceLocation start = clang_getRangeStart(diagRange);
			CXSourceLocation end = clang_getRangeEnd(diagRange);
			clang_getInstantiationLocation(start,0,&range.StartLine,&range.StartColumn,0);
			clang_getInstantiationLocation(end,0,&range.EndLine,&range.EndColumn,0);
			diagnostic.Ranges.push_back(range);
		}
		
		tuDiagnostics.push_back(diagnostic);
		clang_disposeString(string);
	}
	diagnostics[file] = tuDiagnostics;
}
