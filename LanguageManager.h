/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Raffael Zica <raffael@trollgames.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LANGUAGEMANAGER_H
#define LANGUAGEMANAGER_H

#include <string>
#include <vector>
#include <algorithm>
#include <map>

enum DiagnosticType {
	Ignored = 0,
	Note    = 1,
	Warning = 2,
	Error   = 3,
	Fatal   = 4
};

struct Range
{
	unsigned int StartLine, StartColumn;
	unsigned int EndLine, EndColumn;
};

struct Diagnostic
{
	std::string Text;
	std::string Spelling;
	DiagnosticType Type;
	unsigned int Line;
	unsigned int Column;
	std::vector<Range> Ranges;
};

struct UnsavedFile
{
	std::string Name;
	std::string Data;
};

class LanguageManager
{
public:
	virtual void Parse(std::string file) {}
public:
	std::vector<Diagnostic> GetDiagnostics(std::string file) { return diagnostics[file]; }
protected:
	std::map<std::string,std::vector<Diagnostic>> diagnostics;
};

#endif // LANGUAGEMANAGER_H
