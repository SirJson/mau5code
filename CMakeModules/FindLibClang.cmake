# - Try to find libclang
# Once done this will define
#
#  LIBCLANG_FOUND - system has mono
#  LIBCLANG_INCLUDE_DIR - the include directory
#  LIBCLANG_LIBRARY - The mono library

SET(LibClang_INC_SEARCH_PATH
    /usr/lib/pkgconfig/../../include/clang-c
    /usr/include)

SET(LibClang_LIB_SEARCH_PATH
    /usr/lib/pkgconfig/../../lib
    /usr/lib)

FIND_PATH(LIBCLANG_INCLUDE_DIR clang-c/Index.h
          ${LibClang_INC_SEARCH_PATH})

FIND_LIBRARY(LIBCLANG_LIBRARY NAMES clang PATH ${LibClang_LIB_SEARCH_PATH})

IF(LIBCLANG_INCLUDE_DIR AND LIBCLANG_LIBRARY)
	SET(LIBCLANG_FOUND TRUE)
	MESSAGE("-- Found libclang: ${LIBCLANG_INCLUDE_DIR}")
ELSE(LIBCLANG_INCLUDE_DIR AND LIBCLANG_LIBRARY)
	SET(LIBCLANG_FOUND FALSE)
ENDIF(LIBCLANG_INCLUDE_DIR AND LIBCLANG_LIBRARY)