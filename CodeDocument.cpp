/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Raffael Zica <raffael@trollgames.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CodeDocument.h"
#include "qscilexer.h"
#include "qscilexercpp.h"
#include "qsciapis.h"
#include "Utility.h"
#include <QtGui>
#include "LanguageManager.h"
#include "Mau5Code.h"

CodeDocument::CodeDocument (Mau5Code* mainApp, LanguageManager* langMgr)
{
	this->mainApp = mainApp;
	parentWindow = mainApp->GetUserInterface();
	this->langMgr = langMgr;
	this->init();
	parentWindow->editorTabs->setTabText(parentWindow->editorTabs->indexOf(page), "Untitled");
	this->filename = "";
}

CodeDocument::CodeDocument(Mau5Code* mainApp, LanguageManager* langMgr, std::string file)
{
	this->mainApp = mainApp;
	parentWindow = mainApp->GetUserInterface();
	this->langMgr = langMgr;
	this->init();
	this->filename = file;
	this->load();
}

CodeDocument::~CodeDocument()
{
	/*
	parserThread->detach();
	delete parserThread;*/
}

void CodeDocument::init()
{
	page = new QWidget();
	page->setObjectName(QString::fromUtf8("editorPage"));
	layout = new QHBoxLayout(page);
	layout->setObjectName(QString::fromUtf8("editorPageLayout"));
	textEdit = new QsciScintilla(page);
	textEdit->setObjectName(QString::fromUtf8("textEdit"));
	QsciLexerCPP* lexer = new QsciLexerCPP();
	QFont font = QFont("Courier");
	font.setPointSize(10);
	lexer->setFont(font);
	
	QsciAPIs* api = new QsciAPIs(lexer);
	api->add("aLongString");
	api->prepare();
	
	textEdit->setLexer(lexer);
	textEdit->setAutoCompletionThreshold(1);
	textEdit->setAutoCompletionSource(QsciScintilla::AcsAPIs);
	textEdit->setMarginWidth(0,20);
	textEdit->setAnnotationDisplay(QsciScintilla::AnnotationBoxed);
	textEdit->indicatorDefine(QsciScintilla::SquiggleIndicator,1);
	textEdit->setIndicatorForegroundColor(QColor(255,0,0),1);
	
	layout->addWidget(textEdit);
	
	parentWindow->editorTabs->addTab(page, QString());
	
	connect(textEdit, SIGNAL(textChanged()),this, SLOT(documentWasModified()));
	connect(textEdit, SIGNAL(indicatorClicked(int,int,Qt::KeyboardModifiers)),this, SLOT(errorIndicatorClicked(int,int,Qt::KeyboardModifiers)));
	connect(textEdit, SIGNAL(cursorPositionChanged(int,int)),this, SLOT(cursorPositionChanged(int,int)));
	
	//parserThread = new std::thread(&CodeDocument::parseDocument,this);
}

void CodeDocument::documentWasModified()
{
//buildDiagnostics();
}

void CodeDocument::buildDiagnostics()
{
	//parentWindow->errorList->clear();
	for(Diagnostic diagnostic : diagnostics)
	{
		QString str = QString::fromStdString(diagnostic.Text);
		QListWidgetItem* item = &str;
		delete parentWindow->errorList->takeItem(parentWindow->errorList->row(item);
	}
	textEdit->clearAnnotations();
	for(int i = 0; i != diagnostics.size(); i++) {
		textEdit->clearIndicatorRange(diagnostics[i].Line - 1,0,diagnostics[i].Line - 1,textEdit->lineLength(diagnostics[i].Line - 1),1);
	}
	
	diagnostics = langMgr->GetDiagnostics(this->filename);
	for(int i = 0; i != diagnostics.size(); i++) {
		parentWindow->errorList->addItem(diagnostics[i].Text.c_str());
		if(diagnostics[i].Ranges.size() > 0)
		{
			for(int rangeIndex = 0; rangeIndex != diagnostics[i].Ranges.size(); rangeIndex++) {			
				Annotation annotation;
				annotation.Text = DiagnosticTypeToString(diagnostics[i].Type) + diagnostics[i].Spelling;
				annotation.StartLine = diagnostics[i].Ranges[rangeIndex].StartLine - 1;
				annotation.EndLine = diagnostics[i].Ranges[rangeIndex].EndLine - 1;
				annotation.StartColumn = diagnostics[i].Ranges[rangeIndex].StartColumn - 1;
				annotation.EndColumn = diagnostics[i].Ranges[rangeIndex].EndColumn - 1;
				
				textEdit->fillIndicatorRange(	annotation.StartLine,
								annotation.StartColumn,
								annotation.EndLine,
								annotation.EndColumn, 1);
				this->annotations.push_back(annotation);
			}
		}
		else
		{
				Annotation annotation;
				annotation.Text = DiagnosticTypeToString(diagnostics[i].Type) + diagnostics[i].Spelling;
				annotation.StartLine = diagnostics[i].Line - 1;
				annotation.EndLine = diagnostics[i].Line - 1;
				annotation.StartColumn = diagnostics[i].Column - 1;
				annotation.EndColumn = textEdit->lineLength(diagnostics[i].Line - 1);
				
				textEdit->fillIndicatorRange(	annotation.StartLine,
								annotation.StartColumn,
								annotation.EndLine,
								annotation.EndColumn, 1);
				
				this->annotations.push_back(annotation);
		}
	}
}

void CodeDocument::errorIndicatorClicked(int line, int index, Qt::KeyboardModifiers state)
{
	if(state.testFlag(Qt::ControlModifier))
	{
		for(int i = 0; i != annotations.size(); i++) {
			if(line >= annotations[i].StartLine && line <= annotations[i].EndLine && index >= annotations[i].StartColumn && index <= annotations[i].EndColumn)
			{
				textEdit->annotate(line,this->annotations[i].Text.c_str(),1);
				return;
			}
		}	
	}
	textEdit->clearAnnotations();
}

void CodeDocument::cursorPositionChanged(int line, int index)
{
	Qt::KeyboardModifiers state = QApplication::keyboardModifiers();
	if(!state.testFlag(Qt::ControlModifier))
		textEdit->clearAnnotations();
}

void CodeDocument::parseDocument()
{
	/*
	while(true)
	{
		if(this->filename != "")
		{
			std::lock_guard<std::mutex> lck(langMgrMutex); 
			if(!langMgr->FileRegistered(this->filename))
				langMgr->AddFile(this->filename);
			langMgr->Parse();
		}
		sleep(1);
	}
	*/
}

bool CodeDocument::Save()
{
	QString fileName = QString::fromStdString(this->filename);
	QFile file(fileName);
	if (!file.open(QFile::WriteOnly)) {
		QMessageBox::warning(this->mainApp, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
		return false;
	}

	QTextStream out(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	out << textEdit->text();
	QApplication::restoreOverrideCursor();
	file.close();
	langMgr->Parse(this->filename);
	this->buildDiagnostics();
	parentWindow->statusbar->showMessage(tr("File saved"), 2000);
	return true;
}

bool CodeDocument::load()
{
	QString fileName = QString::fromStdString(this->filename);
	QFile file(fileName);
	if (!file.open(QFile::ReadOnly)) {
		QMessageBox::warning(this->mainApp, tr("Application"),
				tr("Cannot read file %1:\n%2.")
				.arg(fileName)
				.arg(file.errorString()));
		return false;
	}

	QTextStream in(&file);
	QApplication::setOverrideCursor(Qt::WaitCursor);
	textEdit->setText(in.readAll());
	QApplication::restoreOverrideCursor();
	file.close();
	parentWindow->editorTabs->setTabText(parentWindow->editorTabs->indexOf(page), this->filename.c_str());
	langMgr->Parse(this->filename);
	this->buildDiagnostics();
	parentWindow->statusbar->showMessage(tr("File loaded"), 2000);
	return true;
}


#include "CodeDocument.moc"
