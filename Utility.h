#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include "LanguageManager.h"

std::string ReadFileToString(std::string file);
std::string WriteFileFromString(std::string data, std::string file);
std::string DiagnosticTypeToString(DiagnosticType type);

#endif